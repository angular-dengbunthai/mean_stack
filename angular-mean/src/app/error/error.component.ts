import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
})
export class ErrorComponent implements OnInit {
  errorMessage: string;
  constructor(
    /**
     * get error data information from ErrorInterceptor
     */
    @Inject(MAT_DIALOG_DATA)
    private matData: { errorResponse: HttpErrorResponse }
  ) {}
  ngOnInit(): void {
    this.errorMessage = this.matData.errorResponse.error.error.message;
  }
}
