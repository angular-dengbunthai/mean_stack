import { Component, OnInit } from '@angular/core';
import { AuthService } from './signup/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor(private authService: AuthService) {}
  title = 'angular-mean';

  ngOnInit(): void {
    // throw new Error('Method not implemented.');
    // alert('sss');
    this.authService.autoAuthUser();
  }

  // storePosts = [];

  // public postEvent(post): void {
  //   this.storePosts.push(post);
  // }
}
