import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../signup/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  isLoading: boolean;
  email = 'test@gmail.com';
  password = '123456';

  authSubject: Subscription;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    // this.isLoading = true;
    this.authSubject = this.authService.getIsAuthenticated().subscribe((isAuthenticated) => {
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    this.authSubject.unsubscribe();
  }

  onLogin(form: NgForm): void {
    this.isLoading = true;
    this.authService.login(form.value.email, form.value.password);
  }
}
