import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent implements OnInit, OnDestroy {
  isLoading;
  private authSubject: Subscription;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    // this.isLoading = true;
    this.authSubject = this.authService
      .getIsAuthenticated()
      .subscribe((isAuthenticated) => {
        this.isLoading = false;
      });
  }

  ngOnDestroy(): void {
    this.authSubject.unsubscribe();
  }

  onSignUp(form: NgForm): void {
    this.isLoading = true;
    const email = form.value.email;
    const password = form.value.password;

    if (!(email && password)) {
      return;
    }

    this.authService.signup(email, password).subscribe((data) => {
      this.isLoading = false;
      form.resetForm();
    });
  }
}
