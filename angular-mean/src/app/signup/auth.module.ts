import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { LoginComponent } from '../login/login.component';
import { AuthRouting } from './auth.routing';
import { SignupComponent } from './signup.component';

@NgModule({
  declarations: [LoginComponent, SignupComponent],
  imports: [CommonModule, FormsModule, AngularMaterialModule],
  exports: [LoginComponent, SignupComponent, AuthRouting],
})
export class AuthModule {}
