import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { SignUp } from './SignUp.model';
import { Token } from './Token.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {}

  token: string;
  userId: string;
  isAuthenticated;
  isAuthenticatedListener = new Subject<boolean>();

  signup(email: string, password: string): Observable<SignUp> {
    const signup: SignUp = {
      email,
      password,
    };
    return this.http.post<SignUp>(
      'http://localhost:3000/api/users/signup',
      signup
    );
  }

  getToken(): string {
    return this.token;
  }

  getIsAuthenticated(): Observable<boolean> {
    return this.isAuthenticatedListener.asObservable();
  }

  login(email: string, password: string): void {
    const login = {
      email,
      password,
    };
    this.http
      .post('http://localhost:3000/api/users/login', login)
      .subscribe((responseToken: Token) => {
        this.token = responseToken.token;
        this.userId = responseToken.userId;
        const expiredTime = responseToken.expiredTime;
        const now = new Date();
        const expirationDate = new Date(now.getTime() + expiredTime * 1000);

        this.setAuthTimer(expiredTime);

        this.saveAuthData(this.token, new Date(expirationDate), this.userId);

        this.isAuthenticated = true;
        this.isAuthenticatedListener.next(true);
        this.router.navigate(['/']);
      }, error => {
        this.isAuthenticatedListener.next(false);
      });
  }
  logout(): void {
    this.token = null;
    this.userId = null;
    this.isAuthenticated = false;
    this.isAuthenticatedListener.next(false);
    this.clearAuthData();
    this.router.navigate(['/']);
  }

  autoAuthUser(): void {
    const tokenInfo = this.getAuthData();
    if (!tokenInfo) {
      return;
    }

    this.token = tokenInfo.token;
    this.userId = tokenInfo.userId;
    const expiredTime = tokenInfo.expiredTime;

    const now = new Date();

    const expiredDuration = expiredTime.getTime() - now.getTime();

    if (expiredDuration <= 0) {
      return;
    }

    this.isAuthenticated = true;
    this.isAuthenticatedListener.next(true);

    this.setAuthTimer(expiredDuration / 1000);
  }

  /**
   * @duration in second
   */
  setAuthTimer(duration: number): void {
    const tokenTimeout = setTimeout(() => {
      this.logout();
      clearTimeout(tokenTimeout);
    }, duration * 1000);
  }

  saveAuthData(token: string, expiredTime: Date, userId: string): void {
    localStorage.setItem('token', token);
    localStorage.setItem('expiredTime', expiredTime.toISOString());
    localStorage.setItem('userId', userId);
  }

  clearAuthData(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('expiredTime');
    localStorage.removeItem('userId');
  }

  getAuthData(): { token: string; expiredTime: Date; userId: string } {
    const token = localStorage.getItem('token');
    const expiredTime = localStorage.getItem('expiredTime');
    const userId = localStorage.getItem('userId');
    if (!token && !expiredTime) {
      return;
    }
    return { token, expiredTime: new Date(expiredTime), userId };
  }
}
