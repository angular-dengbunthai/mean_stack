export interface Token {
  token: string;
  expiredTime: number;
  userId: string;
}
