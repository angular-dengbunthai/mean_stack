import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/signup/auth.service';
import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit, OnDestroy {
  posts: Post[] = [];
  isLoading = true;
  allPosts;
  private postSub: Subscription;
  private authSub: Subscription;
  isAuthenticated;
  userId: string;

  constructor(private postService: PostService, private auth: AuthService) {}

  ngOnInit(): void {
    this.postService.getPosts();
    this.postSub = this.postService
      .getPostUpdatedListener()
      .subscribe((data) => {
        this.posts = data.posts;
        this.allPosts = data.count;
        this.isLoading = false;
      });
    this.isAuthenticated = this.auth.isAuthenticated;
    this.userId = this.auth.userId;
    console.log('Helloooooooo');

    this.authSub = this.auth
      .getIsAuthenticated()
      .subscribe((isAuthenticated) => {
        this.isAuthenticated = isAuthenticated;
        this.userId = this.auth.userId;
        console.log('Helloooooooo2');
      });
  }

  ngOnDestroy(): void {
    this.postSub.unsubscribe();
    this.authSub.unsubscribe();
  }

  onDelete(postId: string): void {
    this.postService.deletePost(postId);
  }

  onChangePage(e: PageEvent): void {
    const page = e.pageIndex;
    const pageSize = e.pageSize;
    this.postService.getPosts(pageSize, page);
    this.isLoading = true;
  }
}
