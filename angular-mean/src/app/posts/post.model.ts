export interface Post {
  id: string;
  title: string;
  content: string;
  imagePath: string;
  creator: string;
}

export interface PostResponse {
  message: string;
  posts: Post[];
  count: number;
}
