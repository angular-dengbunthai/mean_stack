import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/signup/auth.service';
import { Post } from '../post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss'],
})
export class PostCreateComponent implements OnInit {
  enterTitle = 'Default Title';
  enterContent = 'Default Post...';

  mode: string;
  id: string;
  isLoading = false;
  imageUrl: string | ArrayBuffer;

  form: FormGroup;

  constructor(
    private postService: PostService,
    public route: ActivatedRoute,
    public router: Router,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      content: new FormControl('', [Validators.required]),
      image: new FormControl('', [Validators.required]),
    });

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('id')) {
        this.mode = 'edit';
        this.id = paramMap.get('id');
        this.isLoading = true;
        this.postService.postData.posts.find((m: Post) => {
          if (m.id === this.id) {
            this.isLoading = false;

            this.imageUrl = m.imagePath;
            this.form.setValue({
              title: m.title,
              content: m.content,
              image: m.imagePath,
            });
          }
        });
      } else {
        this.mode = 'create';
      }
    });
  }

  public onImagePicker(event: Event): void {
    const file = (event.target as HTMLInputElement).files[0];
    if (!file) {
      return;
    }

    this.form.patchValue({ image: file });
    this.form.get('image').updateValueAndValidity();

    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.imageUrl = fileReader.result;
    };
    fileReader.readAsDataURL(file);
  }
  public savePost(e): void {
    e.preventDefault();
    if (this.form.invalid) {
      return;
    }

    if (this.mode === 'edit') {
      this.postService.updatePost(
        this.id,
        this.form.value.title,
        this.form.value.content,
        this.form.value.image
      );
    } else {
      this.postService.addPost(
        this.form.value.title,
        this.form.value.content,
        this.form.value.image
      );
      this.form.reset();
    }
    this.router.navigate(['/']);
  }
}
