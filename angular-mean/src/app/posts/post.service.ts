import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Post, PostResponse } from './post.model';

// oaqrDh4uhKyGdPhP

@Injectable({
  providedIn: 'root',
})
export class PostService {
  // posts: Post[] = [];
  postData: PostResponse;
  postsUpdated = new Subject<PostResponse>();

  constructor(private http: HttpClient) {}

  getPosts(pageSize: number = 2, currentPage: number = 0): void {
    const query = `?pageSize=${pageSize}&page=${currentPage}`;

    this.http
      .get<PostResponse>('http://localhost:3000/api/posts' + query)
      .pipe(
        map((a) => {
          return {
            ...a,
            posts: a.posts.map((aa: any) => {
              const aaa = {
                ...aa,
                id: aa._id,
              };
              delete aaa._id;
              return aaa;
            }),
          };
        })
      )
      .subscribe((data) => {
        // this.posts = posts;
        // this.postsUpdated.next([...this.posts]);

        this.postData = data;
        this.postsUpdated.next(this.postData);
      });

    // return [...this.posts];
  }

  getPostUpdatedListener(): Observable<PostResponse> {
    return this.postsUpdated.asObservable();
  }

  addPost(title: string, content: string, image: File): void {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image);
    console.log('create!!!!!!!!!!');

    this.http
      .post<{ message: string }>('http://localhost:3000/api/posts', postData)
      .subscribe(() => {
        this.getPosts();
      });
  }

  updatePost(
    postId: string,
    title: string,
    content: string,
    image: File | string
  ): void {
    let postData: FormData | Post;
    if (typeof image === 'object') {
      postData = new FormData();
      postData.append('id', postId);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image);
    } else {
      postData = {
        id: postId,
        title,
        content,
        imagePath: image,
        creator: null,
      };
    }

    this.http
      .put<{ message: string }>(
        'http://localhost:3000/api/posts/' + postId,
        postData
      )
      .subscribe(() => {
        this.getPosts();
      });
  }

  deletePost(postId: string): void {
    this.http
      .delete('http://localhost:3000/api/posts/' + postId)
      .subscribe(() => {
        this.getPosts();
      });
  }
}
