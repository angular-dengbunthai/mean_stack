import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { AuthService } from '../signup/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  
  isAuthenticated = false;
  isAuthenticatedSubscription: Subscription;

  constructor(private auth: AuthService, private router: Router) {

  }

  ngOnInit(): void { 
    this.isAuthenticated = this.auth.isAuthenticated;
    this.isAuthenticatedSubscription = this.auth.getIsAuthenticated().subscribe((isAuthenticated) => {
      this.isAuthenticated = isAuthenticated;
    })
    // throw new Error('Method not implemented.');
  }
  
  ngOnDestroy(): void {
    this.isAuthenticatedSubscription.unsubscribe();
  }

  logout(){
    this.auth.logout();
    // this.router.navigate(['/']);
  }

}
