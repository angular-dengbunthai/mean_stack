const express = require("express");
const router = express.Router();

const postController = require("../controllers/post.controller");

/**
 * npm install multer
 * Use for file operation
 */
const multer = require("multer");

const checkAuth = require("../middleware/check-auth");

const fileMiddleware = require("../middleware/file.middleware");

router.post(
  "",
  checkAuth,
  multer({ storage: fileMiddleware.storage }).single("image"),
  postController.createPosts
);

router.get("", postController.getPosts);

router.put(
  "/:id",
  checkAuth,
  multer({ storage: fileMiddleware.storage }).single("image"),
  postController.putPosts
);

router.delete("/:id", checkAuth, postController.deletePosts);

module.exports = router;
