/**
 * npm install multer
 * Use for file operation
 */
 const multer = require("multer");

const MIME_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpeg",
  "image/jpg": "jpg",
};

exports.storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error("Invalid mime type");
    if (isValid) {
      error = null;
    }
    cb(null, "./backend/image");
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLowerCase().split(" ").join("/");
    const ext = MIME_TYPE_MAP[file.mimetype];
    const uniqueSuffix = name + "-" + Date.now() + "-" + "." + ext;
    cb(null, uniqueSuffix);
  },
});
