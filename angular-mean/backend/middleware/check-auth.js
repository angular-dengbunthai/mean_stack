const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {

  try {
    const token = req.headers.authorization.split(" ")[1];
    const userDecode = jwt.verify(token, 'mysecret');
    // create user information for accessing resources
    req.userData = {
      userId: userDecode.userId,
      email: userDecode.email
    }
    next();
  } catch (error) {
    res.status(401).json({
      message: "Unauthorize failed!"
    })
  }

}
