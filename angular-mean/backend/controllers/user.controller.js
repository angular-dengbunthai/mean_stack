const User = require("../models/user");

/**
 * npm install bcrypt
 * it's used to encrypt password
 */
const bcrypt = require("bcryptjs");

/**
 * npm install jsonwebtoken
 * it's used to created a token for the use of frontend
 */
const jwt = require("jsonwebtoken");

exports.userSignup = (req, res, next) => {
  bcrypt.hash(req.body.password, 10, (err, hash) => {
    const user = new User({
      email: req.body.email,
      password: hash,
    });
    user
      .save()
      .then((result) => {
        res.status(201).json({
          message: "success!",
          result: result,
        });
      })
      .catch((error) => {
        res.status(500).json({
          error: {
            message: "Signup Failed",
          },
        });
      });
  });
};

exports.userLogin = async (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;

  let fetchedUser;
  User.findOne({ email: email })
    .then((user) => {
      if (!user) {
        return res
          .status(401)
          .json({ message: "Email or password is incorrect" });
      }
      fetchedUser = user;

      //return res.status(200).json({});
      return bcrypt.compare(password, user.password);
    })
    .then((result) => {
      if (!result) {
        return res
          .status(500)
          .json({ message: "Email or password is incorrect" });
      }
      if (fetchedUser) {
        const token = jwt.sign(
          { email: fetchedUser.email, userId: fetchedUser.id },
          "mysecret",
          { expiresIn: "1h" }
        );
        return res
          .status(200)
          .json({ token: token, expiredTime: 3600, userId: fetchedUser.id });
      }
    })
    .catch((err) => {
      return res.status(401).json({ message: "Login Error" });
    });
};
