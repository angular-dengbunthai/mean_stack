const Post = require("../models/post");

exports.createPosts = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    imagePath: url + "/image/" + req.file.filename,
    creator: req.userData.userId,
  });
  console.log(req.userData);
  // return res.status(200).json({});
  post.save().then((createPost) => {
    // console.log(createPost);
    res.status(201).json({
      message: "create success",
      post: {
        id: createPost._id,
        title: createPost.title,
        content: createPost.content,
        imagePath: createPost.imagePath,
      },
    });
  });
};

exports.getPosts = (req, res, next) => {
  let pageSize = +req.query["pageSize"];
  let currentPage = +req.query["page"];
  const postFind = Post.find();

  if (req.query["pageSize"] && req.query["page"]) {
    postFind.skip(pageSize * currentPage).limit(pageSize);
  } else {
    pageSize = 2;
    currentPage = 0;
    postFind.skip(pageSize * currentPage).limit(pageSize);
  }

  let documents;
  postFind
    .then((documents) => {
      this.documents = documents;
      return Post.count();
    })
    .then((counts) => {
      // console.log(posts);
      return res.status(200).json({
        message: "fetch successfully",
        posts: this.documents,
        count: counts,
      });
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Created post failed",
      });
    });
};

exports.putPosts = (req, res, next) => {
  let imagePath = req.body.imagePath;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/image/" + req.file.filename;
  }
  console.log("h: " + req.body.id);

  const post = new Post({
    _id: req.body.id,
    title: req.body.title,
    content: req.body.content,
    imagePath: imagePath,
    creator: req.userData.userId,
  });

  Post.updateOne({ _id: req.body.id, creator: req.userData.userId }, post)
    .then((result) => {
      if (result.modifiedCount > 0) {
        return res.status(200).json({
          message: "update successfully",
        });
      } else {
        return res.status(404).json({
          message: "Not found",
        });
      }
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Update post failed",
      });
    });
};

exports.deletePosts = (req, res, next) => {
  console.log("Delete:", req.userData.userId);
  Post.deleteOne({ _id: req.params.id, creator: req.userData.userId })
    .then((result) => {
      // check if delete success
      if (result.deletedCount > 0) {
        console.log("Delete: ", result);
        return res.status(200).json({ message: "delete successfully" });
      } else {
        console.log("Delete: ", result);
        return res.status(401).json({ message: "Authentication failed" });
      }
    })
    .catch((err) => {
      return res.status(500).json({
        message: "Delete post failed",
      });
    });
};
