const express = require("express");

/**
 * npm install mongoose
 * use for connecting & CRUD with MongoDB
 */
const mongoose = require("mongoose");

/**
 * Use for static path image
 */
const path = require("path");

const app = express();

const postRouter = require("./routes/posts");
const userRouter = require("./routes/user");

mongoose
  .connect(
    "mongodb+srv://bunthai:oaqrDh4uhKyGdPhP@cluster0.ejaz0.mongodb.net/node-angular?retryWrites=true&w=majority"
      // "mongodb://localhost:27017/node-angular"
    )
  .then(() => {
    console.log("Connected to MongoDB!!");
  })
  .catch(() => {
    console.log("Failed to connect to MongoDB!!");
  });

app.use(express.json());
app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/posts", postRouter);
app.use("/api/users", userRouter);
app.use("/image", express.static(path.join("backend/image")));

module.exports = app;
