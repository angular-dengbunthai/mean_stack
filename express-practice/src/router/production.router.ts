import express, { NextFunction, Request, Response } from "express";
import Product from "../model/Product.model";
const productRouter = express.Router();

// let products: Product[] = [
//   {
//     code: "KDIENS12XW",
//     name: "Meiji",
//     price: 4,
//     status: "A",
//     category: "Milk",
//   },
//   {
//     code: "KDIENS32XC",
//     name: "formula 1 GXTO",
//     price: 16.5,
//     status: "A",
//     category: "Toy",
//   },
// ];

productRouter.post("/", (req, res, next) => {
  console.log(req.body);

  const product = new Product({
    code: req.body.code,
    name: req.body.name,
    price: req.body.price,
    status: req.body.status,
    category: req.body.category,
    description: req.body.description,
  });
  product.save().then((createdProduct: any) => {
    res.status(201).json({
      message: "create success!",
      product: createdProduct,
    });
  });
});

productRouter.patch("/:code", (req, res, next) => {
  // const product =
  Product.updateOne({ code: req.params["code"] }, req.body).then(
    (updatedProduct: any) => {
      res.status(200).json({ message: "Success!", product: updatedProduct});
    }
  );
});

productRouter.get("/", (req: Request, res: Response, next: NextFunction) => {
  Product.find().then((products) => {
    console.log(products);

    res.status(200).json({
      message: "Success!",
      products: products,
    });
  });
});

productRouter.get(
  "/:code",
  (req: Request, res: Response, next: NextFunction) => {
    Product.findOne({ code: req.params["code"] }).then((product) => {
      res.status(200).json({
        message: "Success!",
        product: product,
      });
    });
  }
);

export default productRouter;
