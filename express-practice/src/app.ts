import express from "express";
import productRouter from "./router/production.router";
import mongoose from 'mongoose';
const app = express();

app.use(express.json());

mongoose.connect(
  
  // "mongodb+srv://bunthai:oaqrDh4uhKyGdPhP@cluster0.ejaz0.mongodb.net/node-angular?retryWrites=true&w=majority"
  'mongodb://localhost/testdb'
  );

mongoose.connection.once('open', () => {
    console.log('connect success!');
    
}).on('error', () =>{
    console.log('connect failed!');
});

// app.connect(mongoose);

app.use('/products', productRouter);

app.listen(5000, () => console.log("Server Running"));
