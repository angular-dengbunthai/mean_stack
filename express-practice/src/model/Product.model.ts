import mongoose from "mongoose";

const productSchema = new mongoose.Schema({
    code: {type: String, required: true},
    name: {type: String, required: true},
    price: {type: Number, required: true},
    status: {type: String, required: true},
    category: {type: String, required: true},
    description: {type: String, required: false}
});

export default mongoose.model('Product', productSchema);
// interface Product {
//     code: string;
//     name: string;
//     price: number;
//     status: string;
//     category: string;
// }

// export default Product;

