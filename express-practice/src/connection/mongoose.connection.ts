import * as mongoose from 'mongoose';

mongoose.connect('mongodb://localhost/testdb');

mongoose.connection.once('open', () => {
    console.log('connect success!');
    
}).on('error', () =>{
    console.log('connect failed!');
});

export default mongoose;